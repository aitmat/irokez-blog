$(function() {
    $('#app_bundle_post_type_title').on('change', function () {
        let inputSlug = $('#app_bundle_post_type_slug');

        let data = {
            'slug' : $(this).val()
        };

        $.post(
            '/admin/generate-slug',
            data,
            function(response) {
                inputSlug.val(response['slug']);
            });
    });
});
