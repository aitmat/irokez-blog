<?php

namespace AppBundle\Controller;

use AppBundle\Form\UserType;
use AppBundle\Service\Slugger;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminController
 * @package AppBundle\Controller
 * @Route("/admin")
 */
class AdminController extends Controller
{
    /**
     * @Route("/", name="admin")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function adminAction()
    {

        return $this->render('admin/admin.html.twig');
    }

    /**
     * @Route("/users", name="admin_users")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function usersAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:User');
        $query = $repository->findAll();

        $paginated  = $this->get('knp_paginator');

        $users = $paginated->paginate(
            $query,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 20)
        );

        return $this->render('admin/adminUsersList.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @Route("/user/edit/{id}", name="admin_user_edit", requirements={"id"="\d+"})
     * @param Request $request
     * @param int $id
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editUser(Request $request, int $id, EntityManagerInterface $entityManager)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:User');
        $user = $repository->find($id);

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $user = $form->getData();
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash(
                'notice',
                'Изменения были успешно сохранены!'
            );

            return $this->redirect($request->server->get('HTTP_REFERER'));
        }

        return $this->render('admin/adminEditUser.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/user/remove/{id}", name="admin_user_remove", requirements={"id"="\d+"})
     * @param EntityManagerInterface $entityManager
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function removeUser(EntityManagerInterface $entityManager, int $id)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:User');
        $post = $repository->find($id);
        if ($post){
            $entityManager->remove($post);
            $entityManager->flush();

            $this->addFlash(
                'notice',
                'Удаление прошло успешно!'
            );
        }
        return $this->redirectToRoute('admin_users');
    }

    /**
     * @param Request $request
     * @param Slugger $slugger
     * @return JsonResponse
     * @Route("/generate-slug", defaults={"_format"="json"})
     */
    public function generateSlugApi(Request $request, Slugger $slugger){
        $response = [];

        $slug = $request->request->get('slug');
        $result = $slugger->wordHandle($slug);
        $response['slug'] = $result;

        $jsonResponse = new JsonResponse();
        $jsonResponse->headers->set('Content-Type', 'application/json');
        $jsonResponse->setData($response);
        return $jsonResponse;
    }
}
