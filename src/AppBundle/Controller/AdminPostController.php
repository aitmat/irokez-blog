<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Form\PostType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminController
 * @package AppBundle\Controller
 * @Route("/admin")
 */
class AdminPostController extends Controller
{
    /**
     * @Route("/posts", name="admin_posts")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Post');
        $query = $repository->findAllSortedByDate();

        $paginated  = $this->get('knp_paginator');

        $posts = $paginated->paginate(
            $query,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 10)
        );

        return $this->render('admin/adminPostList.html.twig', [
            'posts' => $posts,
        ]);
    }

    /**
     * @Route("/create/post", name="new_post_admin")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newPostAction(Request $request, EntityManagerInterface $entityManager)
    {
        $post = new Post();

        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $post = $form->getData();
            $entityManager->persist($post);
            $entityManager->flush();

            $this->addFlash(
                'notice',
                'Успешно выполенена!'
            );

            return $this->redirectToRoute('post_admin', [
                'id' => $post->getId(),
            ]);
        }

        return $this->render('admin/adminCreatePost.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/post/{id}", name="post_admin", requirements={"id"="\d+"})
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showPost(int $id)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Post');
        $post = $repository->find($id);

        return $this->render('admin/adminShowPost.html.twig', [
            'post' => $post,
        ]);
    }

    /**
     * @Route("/post/edit/{id}", name="post_edit_admin", requirements={"id"="\d+"})
     * @param Request $request
     * @param int $id
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editPost(Request $request, int $id, EntityManagerInterface $entityManager)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Post');
        $post = $repository->find($id);

        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $post = $form->getData();
            $entityManager->persist($post);
            $entityManager->flush();

            $this->addFlash(
                'notice',
                'Изменения были успешно сохранены!'
            );

            return $this->redirectToRoute('post_admin', [
                'id' => $post->getId(),
            ]);
        }

        return $this->render('admin/adminEditPost.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/post/remove/{id}", name="post_remove_admin", requirements={"id"="\d+"})
     * @param EntityManagerInterface $entityManager
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function removePost(EntityManagerInterface $entityManager, int $id)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Post');
        $post = $repository->find($id);
        if ($post){
            $entityManager->remove($post);
            $entityManager->flush();

            $this->addFlash(
                'notice',
                'Удаление прошло успешно!'
            );
        }
        return $this->redirectToRoute('admin_posts');
    }
}
