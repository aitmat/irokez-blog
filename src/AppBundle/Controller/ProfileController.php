<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Entity\User;
use AppBundle\Form\PostType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/user")
 * Class ProfileController
 * @package AppBundle\Controller
 */
class ProfileController extends Controller
{
    /**
     * @Route("/", name="user_main")
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $idUser = $this->getUser()->getId();
        $repository = $this->getDoctrine()->getRepository('AppBundle:Post');
        $query = $repository->findAllCreatedByUser($idUser);

        $paginated  = $this->get('knp_paginator');

        $posts = $paginated->paginate(
            $query,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 10)
        );

        return $this->render('profile/user.html.twig', [
            'posts' => $posts,
        ]);
    }

    /**
     * @Route("/post/trash/{id}", name="trash_post", requirements={"id"="\d+"})
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function removeAction(EntityManagerInterface $manager, Request $request, int $id)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Post');
        $post = $repository->find($id);
        $post->setIsRemoved(true);
        $manager->persist($post);
        $manager->flush();
        $this->addFlash(
            'notice',
            'Удаление прошло успешно!'
        );
        return $this->redirect($request->server->get('HTTP_REFERER'));
    }

    /**
     * @Route("/create", name="create_post")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function createPostAction(Request $request, EntityManagerInterface $entityManager)
    {
        $post = new Post();

        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            /** @var Post $post */
            $post = $form->getData();
            $post->setPublicationDate(new \DateTime());
            $post->setAuthor($this->getUser());
            $entityManager->persist($post);
            $entityManager->flush();

            $this->addFlash(
                'notice',
                'Успешно выполенена!'
            );

            return $this->redirectToRoute('user_main');
        }

        return $this->render('profile/createPost.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
