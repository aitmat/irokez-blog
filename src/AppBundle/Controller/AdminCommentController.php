<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\CommentType;
use AppBundle\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminController
 * @package AppBundle\Controller
 * @Route("/admin")
 */
class AdminCommentController extends Controller
{
    /**
     * @Route("/comments", name="admin_comments")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function commentsAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Comment');
        $query = $repository->findAllSortedByDate();

        $paginated  = $this->get('knp_paginator');

        $comments = $paginated->paginate(
            $query,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 20)
        );

        return $this->render('admin/adminCommentsList.html.twig', [
            'comments' => $comments,
        ]);
    }

    /**
     * @Route("/create/user", name="new_user")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newPostAction(Request $request, EntityManagerInterface $entityManager)
    {
        $post = new User();

        $form = $this->createForm(UserType::class, $post);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $post = $form->getData();
            $entityManager->persist($post);
            $entityManager->flush();

            $this->addFlash(
                'notice',
                'Успешно выполенена!'
            );

            return $this->redirectToRoute('admin_users');
        }

        return $this->render('admin/adminCreatePost.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/comment/edit/{id}", name="admin_comment_edit", requirements={"id"="\d+"})
     * @param Request $request
     * @param int $id
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editComment(Request $request, int $id, EntityManagerInterface $entityManager)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Comment');
        $comment = $repository->find($id);

        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $comment = $form->getData();
            $entityManager->persist($comment);
            $entityManager->flush();

            $this->addFlash(
                'notice',
                'Изменения были успешно сохранены!'
            );

            return $this->redirect($request->server->get('HTTP_REFERER'));
        }

        return $this->render('admin/adminEditComment.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/comment/remove/{id}", name="admin_comment_remove", requirements={"id"="\d+"})
     * @param EntityManagerInterface $entityManager
     * @param Request $request
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function removeComment(EntityManagerInterface $entityManager, Request $request, int $id)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Comment');
        $post = $repository->find($id);
        if ($post){
            $entityManager->remove($post);
            $entityManager->flush();

            $this->addFlash(
                'notice',
                'Удаление прошло успешно!'
            );
        }
        return $this->redirect($request->server->get('HTTP_REFERER'));
    }
}
