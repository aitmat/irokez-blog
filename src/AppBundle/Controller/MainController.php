<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MainController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Post');
        $query = $repository->findAllSortedByDate();

        $paginated  = $this->get('knp_paginator');

        $posts = $paginated->paginate(
            $query,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 10)
        );

        return $this->render('index.html.twig', [
            'posts' => $posts,
        ]);
    }

    /**
     * @Route("/read/{id}", name="read_post", requirements={"id"="\d+"})
     * @param int $id
     * @return Response
     */
    public function readPostAction($id)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Post');
        $post = $repository->find($id);

        return $this->render('post.html.twig', [
            'post' => $post,
        ]);
    }
}
