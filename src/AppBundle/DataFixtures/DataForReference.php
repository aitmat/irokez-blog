<?php

namespace AppBundle\DataFixtures;

class DataForReference
{
    static function getUserReferenceKey()
    {
        $userData = UserFixtures::getUserData();
        $key = array_rand($userData);
        return $userData[$key]['email'];
    }
}