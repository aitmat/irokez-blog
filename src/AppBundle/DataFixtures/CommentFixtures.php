<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Post;
use AppBundle\Service\Slugger;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;


class CommentFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @var Slugger
     */
    private $slugger;

    public function __construct(Slugger $slugger)
    {
        $this->slugger = $slugger;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('ru_RU');

        for($i = 0; $i < 1000; $i++){
            $comment = new Comment();
            $comment->setContent($faker->realText('150'));

            /** @var Post $post */
            $post = $this->getReference(PostFixtures::POST_REFERENCE.rand(0, 100));
            $comment->setPost($post);

            $author = $this->getReference(DataForReference::getUserReferenceKey());
            $comment->setAuthor($author);

            $comment->setPublicationDate($faker->dateTimeBetween($post->getPublicationDate()));

            $manager->persist($comment);
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            PostFixtures::class,
        );
    }
}