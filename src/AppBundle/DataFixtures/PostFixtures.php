<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Post;
use AppBundle\Service\Slugger;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class PostFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @var Slugger
     */
    private $slugger;

    const POST_REFERENCE = 'Post';

    public function __construct()
    {
        $this->slugger = new Slugger();
    }

    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('ru_RU');

        for($i = 0; $i < 113; $i++){
            $post = new Post();
            $post->setTitle($faker->realText(100));
            $post->setContent($faker->realText(500));
            $post->setPublicationDate($faker->dateTimeBetween('-2 years'));

            $author = $this->getReference(DataForReference::getUserReferenceKey());
            $post->setAuthor($author);

            $slug = $faker->realText(30);
            $post->setSlug($this->slugger->wordHandle($slug));

            $manager->persist($post);
            $this->addReference(self::POST_REFERENCE.$i, $post);
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
        );
    }
}