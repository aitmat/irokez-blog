<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $usersData = self::getUserData();
        foreach ($usersData as $userData){
            $user = new User();

            $user->setUsername($userData['username']);
            $user->setSurname($userData['surname']);
            $user->setEmail($userData['email']);

            if ($userData['email'] == 'Admin@gmail.com'){
                $user->addRole('ROLE_ADMIN');
            }

            $encoded = $this->encoder->encodePassword($user, $userData['pass']);
            $user->setPassword($encoded);


            $manager->persist($user);
            $this->addReference($userData['email'], $user);
        }
        $manager->flush();
    }

    static function getUserData()
    {
        $array = [
            ['username' => 'Вова', 'surname' => 'Алексеев', 'email' => 'vova@gmail.com', 'pass' => '12345'],
            ['username' => 'Егор', 'surname' => 'Семёнов', 'email' => 'egor@gmail.com', 'pass' => '12345'],
            ['username' => 'Кира', 'surname' => 'Морозов', 'email' => 'kira@gmail.com', 'pass' => '12345'],
            ['username' => 'Макс', 'surname' => 'Степанов', 'email' => 'maks@gmail.com', 'pass' => '12345'],
            ['username' => 'Бека', 'surname' => 'Асанов', 'email' => 'beka@gmail.com', 'pass' => '12345'],
            ['username' => 'Admin', 'surname' => 'Admin', 'email' => 'Admin@gmail.com', 'pass' => '12345'],
        ];
        return $array;
    }
}