<?php
namespace AppBundle\Service;

class Slugger
{
    public function wordHandle($word)
    {
        $word = mb_strtolower($word);
        $word = $this->wordTranslation($word);
        $word = trim($word);
        $word = preg_replace(['# #', '#[.,!?:…]#', '/[^ -~]/'], ['-', '', ''], $word);
        $result = $word;
        return $result;
    }

    function wordTranslation($str)
    {
        $rus = [
            'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л',
            'м', 'н', 'о', 'п', 'р', 'с',
            'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'
        ];

        $lat = [
            'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm',
            'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y',
            'y', 'y', 'e', 'yu', 'ya'
        ];

        return str_replace($rus, $lat, $str);
    }
}