<?php

namespace AppBundle\Form;

use AppBundle\Entity\Post;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'attr' => [
                    'id' => 'title',
                ]
            ])
            ->add('slug', TextType::class)
//            ->add('author', EntityType::class, [
//                'placeholder' => 'Выбрать автор',
//                'class' => User::class,
//                'choice_label' => 'username',
//            ])
//            ->add('publicationDate', DateType::class,[
//                'widget' => 'choice',
//                'html5' => false,
//                'attr' => ['class' => 'js-datepicker'],
//            ])
            ->add('content', TextareaType::class, [
                'attr' => [
                    'style' => 'height:200px',
                ]
            ])
            ->add('save', SubmitType::class, ['label' => 'Сохранить'])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Post::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_post_type';
    }
}
