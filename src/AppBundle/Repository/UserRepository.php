<?php

namespace AppBundle\Repository;

use Doctrine\ORM\NonUniqueResultException;

/**
 * UserRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class UserRepository extends \Doctrine\ORM\EntityRepository
{
    public function findByEmailAndPassword($email, string $password)
    {
        try {
            return $this->createQueryBuilder('u')
                ->select('u')
                ->where('u.email = :email')
                ->andWhere('u.password = :password')
                ->setParameter('email', $email)
                ->setParameter('password', $password)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
